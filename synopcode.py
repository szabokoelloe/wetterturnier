#!/usr/bin/env python
# -*- coding: utf-8 -*-

# synopcode.py
#
# purpose:  Wetterturnier.de
# author:   Szabolcs KÖLLÖ
# e-mail:   szabolcs.koelloe@uibk.ac.at
# web:      http://imgi.uibk.ac.at/
# created:  13-Aug-2013
# modified:

# Synopcode DOC:
# http://www.met.fu-berlin.de/~stefan/fm12.html

import dateFunctions
import miscFunctions

__author__ = 'szabo'

EMPTYSTRING = ""
DONTUSETHISVALUE = -998
WEATHERCODEDONTUSETHISVALUE = 254

TIME0000 = 0
TIME0600 = 600
TIME1200 = 1200
TIME1800 = 1800

SYNOP2table_name = "synop2"
TAWES2table_name = "tawes2"

NOPRECIPITATIONCODE = "990"
BASEPERIOD12H = "2"
BASEPERIOD06H = "1"

WINDLEADINGNUMBER = "0"
TEMPLEADINGNUMBER = "1"
MAXTEMPLEADINGNUMBER = "1"
MINTEMPLEADINGNUMBER = "2"
DEWPOINTLEADINGNUMBER = "2"
ATMOSPHERICPRESSUERELEADINGNUMBER = "3"
PRESSURESEALEVELLEADINGNUMBER = "4"
PRECIPITATIONLEADINGNUMBER = "6"
WEATHERLEADINGNUMBER = "7"
GUSTLEADINGNUMBER = "910"

FIXLANDSTATION = "AAXX "
DETAILBLOCK333 = "333 "
DETAILBLOCK555 = "555 "

# ---------------------------------------------------------------------------
# create synopcode for sunshine duration
# ---------------------------------------------------------------------------
def create_sunshineduration_code(dbHandle, table_name,
                                 statnr, date, time, debugFlag):
    if int(time) == TIME0600:
        cursor = dbHandle.cursor()

        if table_name == SYNOP2table_name:
            query = \
                "SELECT sonnetag " \
                "FROM " + SYNOP2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin = %s " \
                "AND sonnetag < %s;"

            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date)),
                                   TIME0000,
                                   WEATHERCODEDONTUSETHISVALUE))
            if debugFlag: print cursor._executed
            row = cursor.fetchone()
            if row:
                sumSun = row[0]
            else:
                return EMPTYSTRING

        else:
            query = \
                "SELECT sum(so) sumsonne " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s;"

            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date))))
            if debugFlag: print cursor._executed
            row = cursor.fetchone()
            if row:
                sumSun = row[0]
            else:
                return EMPTYSTRING

            if sumSun == None:
                return EMPTYSTRING
            
            sumSun = int(float(sumSun/360) + 0.5)

        return "55" + str(miscFunctions.add_zeros(sumSun, 3)) + " "

    return EMPTYSTRING

# ---------------------------------------------------------------------------
# create synopcode for weather
# ---------------------------------------------------------------------------
def create_weather_code(dbHandle, table_name, statnr, date, time, debugFlag):
    if table_name == SYNOP2table_name:
        maxW1 = 0
        maxW2 = 0

        cursor = dbHandle.cursor()
        if int(time) == TIME1200:
            query = \
            "SELECT max(W1) maxW1 " \
            "FROM " + SYNOP2table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin > %s " \
            "AND stdmin <= %s " \
            "AND W1 <> %s;"

            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date)),
                                   TIME0600,
                                   TIME1200,
                                   WEATHERCODEDONTUSETHISVALUE))
            if debugFlag: print cursor._executed
            row = cursor.fetchone()
            if row:
                maxW1 = row[0]
            else:
                return EMPTYSTRING

            query = \
            "SELECT max(W2) maxW2 " \
            "FROM " + SYNOP2table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin > %s " \
            "AND stdmin <= %s " \
            "AND W1 <> %s;"

            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date)),
                                   TIME0600,
                                   TIME1200,
                                   WEATHERCODEDONTUSETHISVALUE))
            if debugFlag: print cursor._executed
            row = cursor.fetchone()
            if row:
                maxW2 = row[0]
            else:
                return EMPTYSTRING

        elif int(time) == TIME1800:
            query = \
            "SELECT max(W1) maxW1 " \
            "FROM " + SYNOP2table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND W1 <> %s " \
            "AND stdmin > %s " \
            "AND stdmin <= %s;"

            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date)),
                                   WEATHERCODEDONTUSETHISVALUE,
                                   TIME1200,
                                   TIME1800))
            if debugFlag: print cursor._executed
            row = cursor.fetchone()
            if row:
                maxW1 = row[0]
            else:
                return EMPTYSTRING

            query = \
            "SELECT max(W2) maxW2 " \
            "FROM " + SYNOP2table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND W2 <> %s " \
            "AND stdmin > %s " \
            "AND stdmin <= %s;"

            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date)),
                                   WEATHERCODEDONTUSETHISVALUE,
                                   TIME1200,
                                   TIME1800))
            if debugFlag: print cursor._executed
            row = cursor.fetchone()
            if row:
                maxW2 = row[0]
            else:
                return EMPTYSTRING

        query = \
        "SELECT max(ww) ww " \
        "FROM " + SYNOP2table_name + " " \
        "WHERE statnr = %s " \
        "AND ww <> %s " \
        "AND datum = %s " \
        "AND stdmin > %s " \
        "AND stdmin <= %s;"

        cursor.execute(query, (statnr,
                               254,
                               str(dateFunctions.correct_year(date)),
                               int(time) - 600, 
                               int(time)))
        if debugFlag: print cursor._executed

        row = cursor.fetchone()
        if row:
            ww = row[0]
        else:
            return EMPTYSTRING

        if ww == None:
            return EMPTYSTRING

        if (maxW1 >= 10):
            return EMPTYSTRING

        if (maxW2 >= 10):
            return EMPTYSTRING

        return WEATHERLEADINGNUMBER + \
               miscFunctions.add_zeros(str(ww), 2) + \
               str(maxW1) + \
               str(maxW2) + \
               " "

    #TAWES2 table has no weathercode
    return EMPTYSTRING

# ---------------------------------------------------------------------------
# laut RETO wird das so geliefert :-)
# ---------------------------------------------------------------------------
def create_indicator_code(table_name):
    if table_name == SYNOP2table_name:
        return "11/// "
    else:
        #in tawes2 table are no informations for clouds and horizontal sight
        return "16/// "

# ---------------------------------------------------------------------------
# create synopcode for precipitation
# ---------------------------------------------------------------------------
def precipitationCode(dbHandle, table_name, statnr, date, time, debugFlag):
    sum = 0
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        participationcolumn_name = "RRR"
        
        query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + SYNOP2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin = %s " \
                "AND " + participationcolumn_name + " <> %s;"
    
        cursor.execute(query, (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time),
                               DONTUSETHISVALUE))
        if debugFlag: print cursor._executed
        
        row = cursor.fetchone()
        if row:
            value = row[0]
        else:
            return EMPTYSTRING
    else:
        #Begin her for TAWES2
        participationcolumn_name = "rr"
        if int(time) == TIME0000:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin = %s " \
                "AND " + participationcolumn_name + " <> %s;"
    
            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(dateFunctions.get_one_day_after_date(date))),
                                   TIME0000,
                                   DONTUSETHISVALUE))
            if debugFlag: print cursor._executed
    
        elif int(time) == TIME0600:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin > %s " \
                "AND " + participationcolumn_name + " <> %s "\
                "UNION "\
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s;"
    
            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(dateFunctions.get_one_day_before_date(date))),
                                   TIME1800,
                                   DONTUSETHISVALUE,
                                   statnr, str(dateFunctions.correct_year(date)),
                                   TIME0600,
                                   DONTUSETHISVALUE))
            if debugFlag: print cursor._executed
    
        elif int(time) == TIME1200:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s;"
    
            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date)),
                                   TIME0600,
                                   TIME1200,
                                   DONTUSETHISVALUE))
            if debugFlag: print cursor._executed
    
        elif int(time) == TIME1800:
    
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s;"
    
            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(date)),
                                   TIME0600,
                                   TIME1800,
                                   DONTUSETHISVALUE))
            if debugFlag: print cursor._executed
    
        else:
            return EMPTYSTRING
    
        rows = cursor.fetchall()
    
        for row in rows:
            sum = sum + float(row[0])
            
        value = sum
        #END here for TAWES2
        
    if (value >= 1):
        value = int(value)
    elif value > 0:
        value = int(NOPRECIPITATIONCODE) + int(value*10)
    else:
        return EMPTYSTRING

    if (value == 0):
        value = NOPRECIPITATIONCODE
    
    value = miscFunctions.add_zeros(value, 3)

    if int(time) == TIME1200 or int(time) == TIME0000:
        basePeriod = BASEPERIOD06H
    else:
        basePeriod = BASEPERIOD12H

    return PRECIPITATIONLEADINGNUMBER + str(value) + basePeriod + " "

# ---------------------------------------------------------------------------
# generic pressureCode
# value in 1/10 Hectopascal 4 or 5 digits
# if given value bigger 10000 value will be flipped
# if value 5 digits long, it will be shortened to 4 digits
# if value shorter then 4 digits, leading zeros will be added.
# ---------------------------------------------------------------------------
def pressureCode(value):
    if not miscFunctions.check_value_empty(value):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_long(value, 5):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_short(value, 4):
        return EMPTYSTRING

    value = miscFunctions.flip_thousand(value, 10000)

    if len(value) == 5:
        value = str(round(float(value)/10.0, 0))

    return miscFunctions.add_zeros(value, 4)

# ---------------------------------------------------------------------------
# create synopcode for atmospheric pressure sea level
# in synop2 column: Pp -> corrected to sea level
# in tawes2 column: pred -> corrected to sea level
# ---------------------------------------------------------------------------
def create_sealevelpressure_code(dbHandle, table_name, statnr, date, time, debugFlag):
    Pp = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = "SELECT Pp " \
            "FROM " + SYNOP2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    elif table_name == TAWES2table_name:
        query = "SELECT pred " \
            "FROM " + TAWES2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    else:
       return EMPTYSTRING

    cursor.execute(query, (statnr,
                           str(dateFunctions.correct_year(date)),
                           int(time)))
    if debugFlag: print cursor._executed
    row = cursor.fetchone()
    if row:
        Pp = row[0]
    else:
        return EMPTYSTRING

    return PRESSURESEALEVELLEADINGNUMBER + pressureCode(str(Pp)) + " "

# ---------------------------------------------------------------------------
# create synopcode for atmospheric pressure
# in synop2 column: Pg -> Rawdata
# in tawes2 column: p -> Rawdata
# ---------------------------------------------------------------------------
def create_atmosphericpressure_code(dbHandle, table_name, statnr, date, time, debugFlag):
    Pg = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = "SELECT Pg " \
            "FROM " + SYNOP2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    elif table_name == TAWES2table_name:
        query = "SELECT p " \
            "FROM " + TAWES2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    else:
       return EMPTYSTRING

    cursor.execute(query, (statnr,
                           str(dateFunctions.correct_year(date)),
                           int(time)))
    if debugFlag: print cursor._executed
    row = cursor.fetchone()
    if row :
        Pg = row[0]
    else:
        return EMPTYSTRING

    return ATMOSPHERICPRESSUERELEADINGNUMBER + pressureCode(str(Pg)) + " "

# ---------------------------------------------------------------------------
# create synopcode for dewpoint
# ---------------------------------------------------------------------------
def create_dewpoint_code(dbHandle, table_name, statnr, date, time, debugFlag):
    Td = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = "SELECT Td " \
            "FROM " + SYNOP2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    elif table_name == TAWES2table_name:
        query = "SELECT rf " \
            "FROM " + TAWES2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    else:
       return EMPTYSTRING

    cursor.execute(query, (statnr,
                           str(dateFunctions.correct_year(date)),
                           int(time)))
    if debugFlag: print cursor._executed
    row = cursor.fetchone()
    if row :
        Td = row[0]

    if not miscFunctions.check_value_empty(Td):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_long(Td, 3):
        return EMPTYSTRING

    return DEWPOINTLEADINGNUMBER + \
           miscFunctions.create_sign(Td, table_name, "rf") + \
           miscFunctions.add_zeros(miscFunctions.invert_value(Td), 3) \
           + " "

# ---------------------------------------------------------------------------
# create synopcode for temperature
# synop2 table values are ready, in tawes2 we have to search for min, max value
# ---------------------------------------------------------------------------
def create_temp_code(dbHandle, table_name, statnr, date, time, debugFlag):
    temperature = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        column_name = "T"
    elif table_name == TAWES2table_name:
        column_name = "tl"
    else:
       return EMPTYSTRING

    query = "SELECT " + column_name + " " \
            "FROM " + table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"

    cursor.execute(query, (statnr,
                           str(dateFunctions.correct_year(date)),
                           int(time)))
    if debugFlag: print cursor._executed

    row = cursor.fetchone()

    if row:
        temperature = row[0]
    else:
        return EMPTYSTRING

    return TEMPLEADINGNUMBER + \
           miscFunctions.create_sign(temperature) + \
           miscFunctions.add_zeros(miscFunctions.invert_value(temperature), 3) \
           + " "

# ---------------------------------------------------------------------------
# create synopcode for max temperature
# synop2 table values are ready, in tawes2 we have to search for min, max value
# ---------------------------------------------------------------------------
def create_max_temp_code(dbHandle, table_name, statnr, date, time, debugFlag):
    temperature = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        if int(time) == TIME1800 or int(time) == TIME0600:
            query = "SELECT Tmax " \
                    "FROM " + SYNOP2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND stdmin = %s"
        else:
            return EMPTYSTRING

        cursor.execute(query, (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time)))
        if debugFlag: print cursor._executed

    elif table_name == TAWES2table_name:
        if int(time) == TIME1800:
            query = "SELECT max(tlmax) max " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND datum = %s " \
                    "AND stdmin > %s " \
                    "AND stdmin <= %s;"
            cursor.execute(query, (statnr,
                                   dateFunctions.correct_year(date),
                                   TIME0600,
                                   TIME1800))
            if debugFlag: print cursor._executed
        elif int(time) == TIME0600:
            query = "SELECT tlmax " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND stdmin > %s " \
                    "UNION " \
                    "SELECT tlmax " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND stdmin <= %s " \
                    "ORDER BY tlmax DESC LIMIT 1;"
            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(dateFunctions.get_one_day_before_date(date))),
                                   TIME1800,
                                   statnr,
                                   str(dateFunctions.correct_year(date)),
                                   TIME0600))
            if debugFlag: print cursor._executed
        else:
            return EMPTYSTRING

    else:
       return EMPTYSTRING

    row = cursor.fetchone()

    if row:
        temperature = row[0]
    else:
        return EMPTYSTRING
    
    if temperature == None:
        return EMPTYSTRING
    
    if int(temperature) <= DONTUSETHISVALUE:
        return EMPTYSTRING

    return MAXTEMPLEADINGNUMBER + \
           miscFunctions.create_sign(temperature) + \
           miscFunctions.add_zeros(miscFunctions.invert_value(temperature), 3) + \
           " "

# ---------------------------------------------------------------------------
# create synopcode for min temperature
# synop2 table values are ready, in tawes2 we have to search for min, max value
# ---------------------------------------------------------------------------
def create_min_temp_code(dbHandle, table_name, statnr, date, time, debugFlag):
    temperature = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        if int(time) == TIME1800 or int(time) == TIME0600:
            query = "SELECT Tmin " \
                    "FROM " + SYNOP2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND Tmin > %s " \
                    "AND stdmin = %s"
        else:
            return EMPTYSTRING

        cursor.execute(query, (statnr,
                               str(dateFunctions.correct_year(date)),
                               DONTUSETHISVALUE,
                               int(time)))
        if debugFlag: print cursor._executed

    elif table_name == TAWES2table_name:
        if int(time) == TIME1800:
            query = "SELECT min(tlmin) min " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND tlmin > %s " \
                    "AND stdmin > %s " \
                    "AND stdmin <= %s;"
            cursor.execute(query, (statnr,
                                   dateFunctions.correct_year(date),
                                   DONTUSETHISVALUE,
                                   TIME0600,
                                   TIME1800))
            if debugFlag: print cursor._executed
        elif int(time) == TIME0600:
            query = "SELECT tlmin " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND tlmin > %s " \
                    "AND stdmin > %s " \
                    "UNION " \
                    "SELECT tlmin " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND tlmin > %s " \
                    "AND stdmin <= %s " \
                    "ORDER BY tlmin ASC LIMIT 1;"
            cursor.execute(query, (statnr,
                                   str(dateFunctions.correct_year(dateFunctions.get_one_day_before_date(date))),
                                   DONTUSETHISVALUE,
                                   TIME1800,
                                   statnr,
                                   str(dateFunctions.correct_year(date)),
                                   DONTUSETHISVALUE,
                                   TIME0600))
            if debugFlag: print cursor._executed
        else:
            return EMPTYSTRING

    else:
       return EMPTYSTRING

    row = cursor.fetchone()

    if row:
        temperature = row[0]
    else:
        return EMPTYSTRING
    
    if temperature == None:
        return EMPTYSTRING
    
    return MINTEMPLEADINGNUMBER + \
           miscFunctions.create_sign(temperature) + \
           miscFunctions.add_zeros(miscFunctions.invert_value(temperature), 3) + \
           " "

# ---------------------------------------------------------------------------
# create synopcode for wind
# ---------------------------------------------------------------------------
def createWindCode(dbHandle, table_name, statnr, date, time, debugFlag):
    ff = EMPTYSTRING
    cursor = dbHandle.cursor()

    query = "SELECT ff " \
            "FROM " + table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s;"

    cursor.execute(query, (statnr,
                           str(dateFunctions.correct_year(date)),
                           int(time)))
    if debugFlag: print cursor._executed
    row = cursor.fetchone()
    if row :
        ff = row[0]

    if not miscFunctions.check_value_empty(ff):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_long(ff, 3):
        return EMPTYSTRING

    return WINDLEADINGNUMBER + "0" + miscFunctions.add_zeros(ff, 3) + " "

# ---------------------------------------------------------------------------
# create synopcode for cloudscoverlevel, winddirection, windspeed
# in TAWES2 cloudscoverlevel not observed
# ---------------------------------------------------------------------------
def create_covering_winddirection_speed_code(dbHandle, table_name, statnr, date, time, debugFlag):
    N = EMPTYSTRING
    dd = EMPTYSTRING
    ff = EMPTYSTRING
    OOfff = EMPTYSTRING

    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = "SELECT N, dd, ff " \
            "FROM " + SYNOP2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    elif table_name == TAWES2table_name:
        query = "SELECT N, dd, ff " \
            "FROM " + TAWES2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    else:
       return EMPTYSTRING

    cursor.execute(query, (statnr,
                           str(dateFunctions.correct_year(date)),
                           int(time)))
    if debugFlag: print cursor._executed
    row = cursor.fetchone()
    
    if row :
        N, dd, ff = row

    if not miscFunctions.check_value_empty(N):
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(dd):
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(ff):
        return EMPTYSTRING

    ff = int(round(float(ff)/10, 0))

    if int(ff) > 99:
        ff = "//" # no definition in SYNOPcode, RETO say // :-)
        OOfff = createWindCode(dbHandle, table_name, statnr, date, time, debugFlag) + " "

    if int(N) > 9:
        N = "/"

    if table_name == TAWES2table_name:
        N = "/"

    dd = str(int(dd/10.0 + 0.5))

    return str(N) + \
           miscFunctions.add_zeros(dd, 2) + \
           miscFunctions.add_zeros(ff, 2) + \
           " " + \
           OOfff

# ---------------------------------------------------------------------------
# create synopcode for gust
# ---------------------------------------------------------------------------
def create_gust_code(dbHandle, table_name, statnr, date, time, debugFlag):
    boe = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        column_name = "w1boe"
    elif table_name == TAWES2table_name:
        column_name = "ffx"
    else:
       return EMPTYSTRING

    if int(time) == TIME0000:
        query = \
            "SELECT max(" + column_name + ") max " \
            "FROM " + table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin > %s " \
            "AND " + column_name + " <> %s " \
            "UNION " \
            "SELECT " + column_name + " max " \
            "FROM " + table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s " \
            "AND " + column_name + " <> %s " \
            "ORDER BY max DESC LIMIT 1;"

        cursor.execute(query, (statnr,
                               str(dateFunctions.correct_year(dateFunctions.get_one_day_before_date(date))),
                               TIME1800,
                               DONTUSETHISVALUE,
                               statnr,
                               str(dateFunctions.correct_year(date)),
                               TIME0000,
                               DONTUSETHISVALUE))
        if debugFlag: print cursor._executed

    else:
        query = "SELECT max(" + column_name + ") " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND " + column_name + " <> %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s;"

        cursor.execute(query, (statnr,
                               str(dateFunctions.correct_year(date)),
                               DONTUSETHISVALUE,
                               int(time)-600,
                               int(time)))
        if debugFlag: print cursor._executed

    row = cursor.fetchone()

    if row :
        boe = row[0]
    else:
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(boe):
        return EMPTYSTRING

    if boe is None:
        return EMPTYSTRING

    if int(boe) < 128: # 25 knots -> 12.86 m/s, data is in 1/10 knots in the table
        return EMPTYSTRING

    boe = int(boe/10 + 0.5)

    return GUSTLEADINGNUMBER + \
           miscFunctions.add_zeros(str(boe), 2) + \
           " "

def createSynopCode(date, time, statnr, db_handle, table_name, debug_flag):
    synopstring333 = EMPTYSTRING
    synopstring555 = EMPTYSTRING

    b_IIIII = str(statnr) + " "
    b_iihVV = create_indicator_code(table_name)
    b_Nddff = create_covering_winddirection_speed_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_1sTTT = create_temp_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_2sTTT = create_dewpoint_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_3PPPP = create_atmosphericpressure_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_4PPPP = create_sealevelpressure_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_6RRRt = precipitationCode(db_handle, table_name, statnr, date, time, debug_flag)
    b_7wwWW = create_weather_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_55SSS = create_sunshineduration_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_1xTTT = create_max_temp_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_2xTTT = create_min_temp_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_555_910XX = create_gust_code(db_handle, table_name, statnr, date, time, debug_flag)

    synopstringBasic = \
        date.strftime("%Y%m%d") + miscFunctions.add_zeros(time, 4) + \
        " " + FIXLANDSTATION + \
        date.strftime("%d") + miscFunctions.add_zeros(time, 4)[0:2] + "1 " + \
        b_IIIII  + \
        b_iihVV + \
        b_Nddff + \
        b_1sTTT + \
        b_2sTTT + \
        b_3PPPP + \
        b_4PPPP + \
        b_6RRRt + \
        b_7wwWW

    if b_333_1xTTT != EMPTYSTRING or b_333_2xTTT != EMPTYSTRING or b_333_55SSS != EMPTYSTRING:
        synopstring333 = \
            DETAILBLOCK333 + \
            b_333_1xTTT + \
            b_333_2xTTT + \
            b_333_55SSS

    if b_555_910XX != EMPTYSTRING:
        synopstring555 = \
            DETAILBLOCK555 + \
            b_555_910XX

    print synopstringBasic + synopstring333 + synopstring555
    return synopstringBasic + synopstring333 + synopstring555