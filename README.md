#Configuration in
config.conf
look into the configfile for configuration details.

examle configfile:
config.conf.example

#start - config.conf will be processed
./wetterturnier.py

#start with debugmessages
./wetterturnier.py -v

#other params:
print 'wetterturnier.py -v -d 20130901 -s 11035'

print 'wetterturnier.py -d 20130901 -s 11035'

print 'wetterturnier.py -s 11035'

print 'wetterturnier.py --outputdebugmessages --date 20130901 --station 11035'

print 'wetterturnier.py --date 20130901 --station 11035'

print 'wetterturnier.py --station 11035'

print '!! only stations which are defined in the config.conf work - that means every station needs a defined tablename !!'
