#!/usr/bin/env python
# -*- coding: utf-8 -*-

# database.py
#
# purpose:  Wetterturnier.de
# author:   Szabolcs KÖLLÖ
# e-mail:   szabolcs.koelloe@uibk.ac.at
# web:      http://imgi.uibk.ac.at/
# created:  14-Aug-2013
# modified:

import sys
import MySQLdb

__author__ = 'szabo'

def create_mysql_con(config):
    mysql_opts = {
        'host': config["host"],
        'user': config["user"],
        'pass': config["passw"],
        'db':   config["db"]
    }

    try:
        mysql = MySQLdb.connect(mysql_opts['host'], mysql_opts['user'], mysql_opts['pass'], mysql_opts['db'])
    except MySQLdb.Error, e:
        print "Error %d: %s" % (e.args[0], e.args[1])
        sys.exit (1)

    mysql.apilevel = "2.0"
    mysql.threadsafety = 2
    mysql.paramstyle = "format"

    return mysql