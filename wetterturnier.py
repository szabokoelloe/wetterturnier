#!/usr/bin/env python
# -*- coding: utf-8 -*-

# wetterturnier.py
#
# purpose:  Wetterturnier.de
# author:   Szabolcs KÖLLÖ
# e-mail:   szabolcs.koelloe@uibk.ac.at
# web:      http://imgi.uibk.ac.at/
# created:  12-Aug-2013
# modified:


import database
import sys

import getopt #www.tutorialspoint.com/python/python_command_line_arguments.htm

import datetime
import dateFunctions
import synopcode
import miscFunctions

__author__ = 'szabo'

EMPTYSTRING = ""

if __name__ == "__main__":

    debug_flag = False
    dontUseSaturdaySunday = False

    config = {}
    execfile("config.conf", config)

    mysql = database.create_mysql_con(config)
    stations = config["stations"]

    try:
        opts, args = getopt.getopt(sys.argv[1:],"vd:s:",["outputdebugmessages=","date=","station="])
    except getopt.GetoptError:
        print 'wetterturnier.py -v -d 20130901 -s 11035'
        print 'wetterturnier.py -d 20130901 -s 11035'
        print 'wetterturnier.py -s 11035'
        print 'wetterturnier.py --outputdebugmessages --date 20130901 --station 11035'
        print 'wetterturnier.py --date 20130901 --station 11035'
        print 'wetterturnier.py --station 11035'
        print '!! only stations which are defined in the config.conf work - that means every station needs a defined tablename !!'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-v", "--outputdebugmessages"):
            debug_flag = True

        if opt in ("-d", "--date"):
            processingDate = datetime.date(int(arg[0:4]), int(arg[4:6]), int(arg[6:8]))
            dontUseSaturdaySunday = True

        if opt in ("-s", "--station"):
            stations = [arg]

    if not dontUseSaturdaySunday:
        saturday = dateFunctions.get_last_saturday(datetime.date.today())
        sunday = dateFunctions.get_last_sunday(datetime.date.today())

        saturdayCorrected = dateFunctions.get_corrected_last_saturday(datetime.date.today())
        sundayCorrected = dateFunctions.get_corrected_last_sunday(datetime.date.today())

    for station in stations:
        # BEGIN LOOP for stations
        tableNameString = config["s" + str(station)]

        for deliverytime in config["deliverytimes"]:
            if debug_flag: print "Processing station: " + str(station)
            if debug_flag: print "Processing time: " + str(deliverytime)

            if dontUseSaturdaySunday:
                processDateTime = datetime.datetime.combine(processingDate, miscFunctions.create_time(deliverytime))
                if datetime.datetime.today() > processDateTime:
                    synopcode.createSynopCode(processingDate, str(deliverytime), station, mysql, tableNameString, debug_flag)
            else:
                processDateTime = datetime.datetime.combine(saturday, miscFunctions.create_time(deliverytime))
                if datetime.datetime.today() > processDateTime:
                    if debug_flag: print "Saturday: "
                    synopcode.createSynopCode(saturday, str(deliverytime), station, mysql, tableNameString, debug_flag)

                processDateTime = datetime.datetime.combine(saturday, miscFunctions.create_time(deliverytime))
                if datetime.datetime.today() > processDateTime:
                    if debug_flag: print "Sunday: "
                    synopcode.createSynopCode(sunday, str(deliverytime), station, mysql, tableNameString, debug_flag)
                if debug_flag: print "\n"
            # END LOOP for deliverytimes
        if debug_flag: print "\n"
        # END LOOP for stations

    mysql.close()